package diablo.four.wallpapper.com

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import kotlinx.android.synthetic.main.activity_detail.*
import com.bumptech.glide.Glide
import android.content.DialogInterface
import android.os.Build
import androidx.appcompat.app.AlertDialog
import android.widget.Toast
import android.graphics.drawable.BitmapDrawable
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.app.WallpaperManager
import java.io.IOException


class DetailActivity : AppCompatActivity() {

    private val handler: Handler? = null
    private var rsImage: Int = 0
    private var alertDialog: AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        getIntentValue()
        rlBottom.setOnClickListener(View.OnClickListener {
            setGallery()
        })
    }

    private fun setImage() {
        Glide.with(this).load(rsImage).into(imvFullScreen)
    }

    private fun getIntentValue() {
        val bundle = intent.extras
        if (bundle != null) {
            rsImage = bundle.getInt(MainActivity.INTENT_IMAGE)
            setImage()
        }
    }

    fun setGallery() {
        val builder: AlertDialog.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert)
        } else {
            builder = AlertDialog.Builder(this)
        }
        alertDialog = builder.setTitle("Wallapper")
            .setMessage("Are you sure you want to change Desktop mobile?")
            .setPositiveButton(android.R.string.yes,
                DialogInterface.OnClickListener { dialog, which ->
                    Toast.makeText(this, "Wait.... many second", Toast.LENGTH_LONG).show()
                    alertDialog?.dismiss()
                    setWallpaper(rsImage)
                })
            .setNegativeButton(android.R.string.no,
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.cancel()
                })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    fun setWallpaper(imageDrawble: Int) {
        val wallpaperManager = WallpaperManager.getInstance(this@DetailActivity)
        val drawable = resources.getDrawable(imageDrawble)
        val bitmap = (drawable as BitmapDrawable).bitmap
        wallpaperManager.setBitmap(bitmap)
        Toast.makeText(this, "WallPapper Change Success", Toast.LENGTH_LONG).show()
    }

}
