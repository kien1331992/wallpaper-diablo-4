package diablo.four.wallpapper.com

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), WallpaperAdapter.EvenOnClick {
    override fun onClickItem(rsImage: Int) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(INTENT_IMAGE, rsImage)
        startActivity(intent)
    }

    val MAX_LEN = 42

    companion object {
        val INTENT_IMAGE: String = "uri_image"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var arrImage: ArrayList<String> = ArrayList()
        for (i in 1..MAX_LEN) {
            arrImage.add("diablo_" + i)
        }
        var adapter = WallpaperAdapter(this, arrImage, this)

        rvItem.layoutManager = GridLayoutManager(this,3)
        rvItem.adapter = adapter
    }


}
