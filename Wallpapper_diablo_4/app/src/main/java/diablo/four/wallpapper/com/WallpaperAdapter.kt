package diablo.four.wallpapper.com

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_gallery.view.*

class WallpaperAdapter(val context: Context, val arrImage: List<String>, var even: EvenOnClick) :
    RecyclerView.Adapter<WallpaperAdapter.WallpaperViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): WallpaperViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_gallery, p0, false)
        return WallpaperViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrImage.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(p0: WallpaperViewHolder, p1: Int) {
        Glide.with(context).load(getImage(arrImage.get(p1))).into(p0.imv);
        p0.imv?.setOnClickListener(View.OnClickListener {
            even.onClickItem(getImage(arrImage.get(p1)))
        })

    }


    class WallpaperViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imv = itemView.imv
    }

    fun getImage(imageName: String): Int {
        var drawableResourceId =
            this.context.resources.getIdentifier(imageName, "mipmap", this.context.packageName)
        return drawableResourceId
    }

    interface EvenOnClick {
        fun onClickItem(rsImage: Int)
    }
}